package grupo.seis.poo;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        // Crear una lista de libros y usuarios simulados
        List<Libro> libros = new ArrayList<>();
        List<Usuario> usuarios = new ArrayList<>();

        // Agregar libros y usuarios a las listas (puedes repetir esto para obtener más elementos)

        // Medir tiempo de préstamo para todos los libros
        long startTimePrestamo = System.nanoTime();
        for (Libro libro : libros) {
            libro.prestar();
        }
        long endTimePrestamo = System.nanoTime();
        double durationPrestamo = (endTimePrestamo - startTimePrestamo) / 1e9; // Convertir a segundos

        // Medir tiempo de devolución para todos los libros
        long startTimeDevolucion = System.nanoTime();
        for (Libro libro : libros) {
            libro.devolver();
        }
        long endTimeDevolucion = System.nanoTime();
        double durationDevolucion = (endTimeDevolucion - startTimeDevolucion) / 1e9; // Convertir a segundos

        // Mostrar resultados
        System.out.println("Tiempo de préstamo total: " + durationPrestamo + " segundos");
        System.out.println("Tiempo de devolución total: " + durationDevolucion + " segundos");
    }
}